const mix = require('laravel-mix')
const path = require('path')

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix
  .js('resources/js/index.js', 'public/js')
  .sass('resources/sass/bulma.scss', 'public/css')
  .sass('resources/sass/app.scss', 'public/css')
  .options({
    processCssUrls: false,
    uglify: {
      parallel: 8, // Use multithreading for the processing
      uglifyOptions: {
        mangle: true,
        compress: false // The slow bit
      }
    }
  })
  .extract()

mix.copyDirectory('resources/assets', 'public/assets')

mix.webpackConfig({
  mode: process.env.NODE_ENV === 'production' ? 'production' : 'development',
  resolve: {
    alias: {
      '@': path.resolve(__dirname, 'resources/js'),
      '-': path.resolve(__dirname, 'resources/sass')
    }
  }
})
