<!-- PROJECT LOGO -->
<br />
<div align="center">
  <a href="https://git.thecodingmachine.com/tcm-projects/polytechnique-perk">
    <img src="logo.jpg" alt="Logo" width="200" height="130">
  </a>

  <h3 align="center">Polytechnique Perks</h3>

  <p align="center">
    Polytechnique Perks
  </p>
</div>



<!-- TABLE OF CONTENTS -->
## Table of Contents

* [About the Project](#about-the-project)
  * [Built With](#built-with)
* [Getting Started](#getting-started)
  * [Prerequisites](#prerequisites)
  * [Installation](#installation)
  * [Make Commands](#make-commands)
* [Contact](#contact)
* [Acknowledgements](#acknowledgements)

<!-- ABOUT THE PROJECT -->
## About The Project

### Built With

* [Laravel 6.0](https://laravel.com)
* [VueJS 2.6.10](https://github.com/vuejs/vue)

### Deployment

Deployment is managed with `kubernetes` for all branches except `master` and `develop`  
A specific configuration is made for the `staging` branch : database and storage files will not be deleted for each deployments.

<!-- GETTING STARTED -->
## Getting Started

### Prerequisites

- [Docker](https://www.docker.com/get-started) with [Docker Compose](https://docs.docker.com/compose/install/)
- [Make](https://www.gnu.org/software/make/) (For Windows, use [Make for Windows](http://gnuwin32.sourceforge.net/packages/make.htm) 
or launch ``choco install make`` with [Chocolatey](https://chocolatey.org))

### Installation

To use this application:

- clone the repository 
- launch 
```
cp .env.example .env
docker-compose up
```
- OR launch ``make install`` (that's the same procedure but faster) 

That's it!

#### API & BO

You can start the application by running:

```
docker-compose up -d
yarn
yarn watch
```
or 
``make start && make yarn
``

You can now navigate to `http://perks.localhost`

:warning:
To install a new package inside the `app`, you **MUST** run `composer` or `yarn` **inside** the api container
For this, just launch ``make bash`` or ``make sh`` and then install your packages

## Make Commands
Make commands are really useful for avoiding losing time by typing all the same long commands every time
There are aliases for commands

For example : 

```
make artisan (docker-compose exec app php artisan view:clear)
make artisan CMD=view:clear (docker-compose exec app php artisan view:clear)
make stop (docker-compose stop)
make bash|sh
make bash|sh CONTAINER=mysql
```

| alias    |      Description and usage   |
|---|---|
|``artisan``| Launch artisan command (``make artisan`` or ``make artisan CMD=view:clear``) |
|``bash``| Gives access to container bash (``make bash CONTAINER=mysql`` or ``make bash`` (for app container by default)) |
|``help``| Display commands informations |
|``install``| Install containers and copy env files |
|``restart``| Relaunch the containers application |
|``sh``| Gives access to container bash (``make sh CONTAINER=mysql`` or ``make sh`` (for app container by default)) |
|``start``| Start the containers application |
|``stop``| Stop the containers application |
|``check``| Execute tests & PHPCS & PHPStan & yarn lint |
|``check-js``| Execute only yarn lint |
|``check-php``| Execute only PHP tests & PHPCS & PHPStan |
|``fix-js``| Execute yarn lint fix |

## Contact

- Julien RAVIA - j.ravia@thecodingmachine.com
- Aurélien MUTIN - a.mutin@thecodingmachine.com  
