import Vue from 'vue'
import Vuex from 'vuex'

import common from './modules/common'
import auth from './modules/auth'
import usersList from './modules/usersList'
import createPerk from './modules/createPerk'
import perksList from './modules/perksList'
import perk from './modules/perk'
import collaboration from './modules/collaboration'
import company from './modules/company'

Vue.use(Vuex)

export default new Vuex.Store({
  modules: {
    common,
    auth,
    usersList,
    createPerk,
    perksList,
    perk,
    collaboration,
    company
  }
})
