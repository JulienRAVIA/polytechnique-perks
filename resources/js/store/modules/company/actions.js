import api from '@/services/api'

export default {
  async getCompanies({ commit }) {
    commit('listCompaniesLoading')
    try {
      const response = await api.get('/api/users/companies')
      commit('listCompaniesSuccess', response.data.data)
      return true
    } catch (e) {
      commit('listCompaniesError', e.response.data)
      return false
    }
  }
}
