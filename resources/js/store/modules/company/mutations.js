export default {
  listCompaniesError(state, error) {
    state.companies = []
    state.companiesLoading = false
    state.companiesError = error
  },
  listCompaniesLoading(state) {
    state.companies = []
    state.companiesLoading = true
    state.companiesError = {}
  },
  listCompaniesSuccess(state, list) {
    state.companies = list
    state.companiesLoading = false
  }
}
