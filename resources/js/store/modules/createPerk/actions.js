import api from '@/services/api'

export default {
  async create({ commit }, data) {
    commit('createPerkLoading')
    try {
      const response = await api.post('/api/perks', data)
      commit('createPerkSuccess', response.data)
      return true
    } catch (e) {
      commit('createPerkError', e.response.data)
      return false
    }
  },

  async getCategories({ commit }) {
    commit('categoriesLoading')
    try {
      const response = await api.get('/api/categories')
      commit('categoriesSuccess', response.data.data)
      return true
    } catch (e) {
      commit('categoriesError', e.response)
      return false
    }
  },

  async update({ commit }, data) {
    commit('updatePerkLoading')
    try {
      await api.post('/api/perks/' + data.id, data)
      commit('updatePerkSuccess')
      return true
    } catch (e) {
      commit('updatePerkError', e.response)
      return false
    }
  }
}
