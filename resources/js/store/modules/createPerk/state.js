export default {
  createdPerk: {},
  createPerkLoading: false,
  createPerkError: {},
  categoriesLoading: false,
  categoriesError: false,
  categories: [],
  updatePerkLoading: false,
  updatePerkError: {}
}
