export default {
  createPerkLoading(state) {
    state.createPerkLoading = true
  },
  createPerkSuccess(state, data) {
    state.createdPerk = data.data
    state.createPerkLoading = false
    state.createPerkSuccess = true
  },
  createPerkError(state, error) {
    state.createPerkLoading = false
    state.createPerkError = error
  },
  categoriesLoading(state) {
    state.categoriesLoading = true
  },
  categoriesSuccess(state, list) {
    state.categoriesLoading = false
    state.categories = list
  },
  categoriesError(state) {
    state.categoriesLoading = false
  },
  updatePerkLoading(state) {
    state.updatePerkLoading = true
    state.updatePerkError = {}
  },
  updatePerkSuccess(state) {
    state.updatePerkLoading = false
    state.updatePerkSuccess = true
  },
  updatePerkError(state, error) {
    state.updatePerkLoading = false
    state.updatePerkError = error
  }
}
