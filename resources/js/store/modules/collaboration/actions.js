import api from '@/services/api'

export const list = async ({ commit }) => {
  commit('listLoading')
  try {
    const response = await api.get('/api/collaborations')
    commit('listSuccess', response.data)
  } catch (e) {
    commit('listError', e.response.data)
  }
}

export default {
  list
}
