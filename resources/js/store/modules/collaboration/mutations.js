export default {
  listLoading(state) {
    state.listLoading = true
    state.list = []
    state.listError = null
  },
  listSuccess(state, list) {
    state.listLoading = false
    state.list = list
    state.listError = null
  },
  listError(state, error) {
    state.listLoading = false
    state.list = []
    state.listError = error
  }
}
