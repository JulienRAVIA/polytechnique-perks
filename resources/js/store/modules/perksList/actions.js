import api from '@/services/api'

export default {
  async getPerksList({ commit }, { page }) {
    commit('perksListLoading')
    try {
      const response = await api.get('/api/perks/all', {
        params: { page: page }
      })
      commit('perksListSuccess', response.data)
    } catch (e) {
      commit('perksListError', e.response.data)
    }
  },

  async perkStateChange({ commit }, { perkId, status }) {
    commit('perkStateChangeLoading')
    try {
      const response = await api.post(`/api/perks/${perkId}/validation`, {
        status: status
      })
      commit('perkStateChangeSuccess', response)
      return true
    } catch (e) {
      commit('perkStateChangeError', e.response.data)
      return false
    }
  }
}
