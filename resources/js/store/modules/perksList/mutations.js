export default {
  perksListLoading(state) {
    state.perksListError = {}
    state.perksListLoading = true
  },
  perksListError(state, error) {
    state.perksListLoading = false
    state.perksListError = error
  },
  perksListSuccess(state, data) {
    state.perksListError = {}
    state.list = data
    state.perksListLoading = false
  },
  perkStateChangeLoading(state) {
    state.perkStateChangeLoading = true
  },
  perkStateChangeError(state, error) {
    state.perkStateChangeLoading = false
    state.perkStateChangeError = error
  },
  perkStateChangeSuccess(state) {
    state.perkStateChangeLoading = false
  }
}
