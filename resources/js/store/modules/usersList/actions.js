import api from '@/services/api'

export default {
  async getUsersList({ commit }, { page }) {
    commit('usersListLoading')
    try {
      const response = await api.get(`/api/users`, {
        params: { page: page }
      })
      commit('usersListSuccess', response.data)
    } catch (e) {
      commit('usersListError', e.response.data)
    }
  },

  async userStateChange({ commit }, { userId, status }) {
    commit('userStateChangeLoading')
    try {
      const response = await api.post(`/api/users/${userId}/validation`, {
        status: status
      })
      commit('userStateChangeSuccess', response)
      return true
    } catch (e) {
      commit('userStateChangeError', e.response.data)
      return false
    }
  }
}
