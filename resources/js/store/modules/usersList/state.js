export default {
  list: {},
  usersListLoading: false,
  usersListError: {},
  userStateChangeLoading: false,
  userStateChangeSuccess: null,
  userStateChangeError: null
}
