export default {
  usersListLoading(state) {
    state.usersListError = {}
    state.usersListLoading = true
  },
  usersListError(state, error) {
    state.usersListLoading = false
    state.usersListError = error
  },
  usersListSuccess(state, data) {
    state.usersListError = {}
    state.list = data
    state.usersListLoading = false
  },
  userStateChangeLoading(state) {
    state.userStateChangeLoading = true
  },
  userStateChangeError(state, error) {
    state.userStateChangeLoading = false
    state.userStateChangeError = error
  },
  userStateChangeSuccess(state) {
    state.userStateChangeLoading = false
  }
}
