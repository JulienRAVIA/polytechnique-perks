export default {
  user: null,
  loginError: null,
  loginLoading: false,
  logoutLoading: false,
  logoutError: null,
  meLoading: false,
  meError: null,
  registerError: null,
  registerLoading: false,
  resetLoading: false,
  resetSuccess: null,
  resetError: null
}
