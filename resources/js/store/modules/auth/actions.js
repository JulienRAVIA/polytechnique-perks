import api from '@/services/api'
import router from '@/router'

export const login = async ({ commit }, { email, password }) => {
  commit('loginLoading')
  try {
    const response = await api.post('/api/login', {
      email: email,
      password: password
    })
    commit('loginSuccess', response.data.data)
    return router.push({ name: 'home' })
  } catch (e) {
    commit('loginError', e.response.data)

    if (e.response.status === 401) {
      router.push({ name: 'activation_page' })
    } else if (e.response.status === 403) {
      router.push({ name: 'login_refused' })
    }
  }
}

export const forgot = async ({ commit }, { email }) => {
  commit('resetLoading')
  try {
    const response = await api.post('/api/password/forgot', {
      email: email
    })
    commit('resetSuccess', response.data)
    return { code: 'success', message: response.data }
  } catch (e) {
    commit('resetError', e.response.data)
    return { code: 'danger', message: e.response.data.message }
  }
}

export const reset = async ({ commit }, form) => {
  commit('resetLoading')
  try {
    const response = await api.post('/api/password/reset', {
      email: form.email,
      token: form.token,
      password: form.password,
      password_confirmation: form.password_confirmation
    })
    commit('resetSuccess', response.data)
    router.push({ name: 'home' })
    return { code: 'success', message: response.data }
  } catch (e) {
    commit('resetError', e.response.data)
    return { code: 'danger', message: e.response.data.message }
  }
}

export const register = async ({ commit }, form) => {
  commit('registerLoading')
  try {
    const response = await api.post('/api/register', form, {
      headers: { 'Content-Type': 'multipart/form-data' }
    })
    commit('registerSuccess', response.data.data)
    router.push({ name: 'activation_page' })
    return true
  } catch (e) {
    commit('registerError', e.response.data)
  }
}

export const logout = async ({ commit }) => {
  commit('logoutLoading')
  try {
    await api.post('/api/me/logout')
    commit('logoutSuccess')
    return router.push({ name: 'login' })
  } catch (e) {
    commit('logoutError', e.response)
  }
}

export const me = async ({ commit }) => {
  try {
    const response = await api.get('/api/me')
    commit('meSuccess', response.data.data)
  } catch (e) {
    commit('meError', e.response)
  }
}

export default {
  me,
  login,
  register,
  logout,
  reset,
  forgot
}
