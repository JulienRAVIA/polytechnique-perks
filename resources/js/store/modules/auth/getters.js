export default {
  connectedUser: state => state.user,
  userStatus: state => (state.user ? state.user.status : null),
  isLoggedIn: state => state.user !== null,
  isAdmin: state => (state.user ? state.user.role === 'admin' : null),
  canProposePerk: state =>
    state.user ? state.user.permissions.includes('propose perk') : null,
  canShowPerks: state =>
    state.user ? state.user.permissions.includes('show perks') : null
}
