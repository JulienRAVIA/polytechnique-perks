export default {
  setHeaderTitle(state, title) {
    state.headerTitle = title
  },
  setGlobalLoading(state, loading) {
    state.globalLoading = loading
  },
  toggleCollapse(state) {
    state.asideCollapse = !state.asideCollapse
  },
  setBooting(state, booting) {
    state.booting = booting
  },
  setHeader(state, header) {
    state.header = header
  }
}
