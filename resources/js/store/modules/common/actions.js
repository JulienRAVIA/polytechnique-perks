import { defaultHeader } from './state'

export const setHeaderTitle = ({ commit }, title) => {
  commit('setHeaderTitle', title)
}
export const setGlobalLoading = ({ commit }, loading) => {
  commit('setGlobalLoading', loading)
}
export const toggleAside = ({ commit }) => {
  commit('toggleCollapse')
}

export const setBooting = ({ commit }, booting) => {
  commit('setBooting', booting)
}

export const setHeader = ({ commit }, header) => {
  commit('setHeader', header)
}

export const resetHeader = ({ commit }) => {
  commit('setHeader', defaultHeader)
}

export default {
  setHeaderTitle,
  setGlobalLoading,
  toggleAside,
  setBooting,
  setHeader,
  resetHeader
}
