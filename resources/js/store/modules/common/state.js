export const defaultHeader = {
  title: 'The Platform',
  titleSecondLine: 'for searching partners',
  subtitle: 'incubator & booster',
  img: '/assets/images/header.png',
  ribbon: 'X-PERKS'
}

export default {
  headerTitle: '',
  globalLoading: false,
  asideCollapse: false,
  booting: true,
  header: defaultHeader
}
