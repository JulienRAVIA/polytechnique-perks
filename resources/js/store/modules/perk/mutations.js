export default {
  perkLoading(state) {
    state.perkError = {}
    state.perkLoading = true
  },
  perkError(state, error) {
    state.perkLoading = false
    state.perkError = error
  },
  perkSuccess(state, data) {
    state.perkError = {}
    state.perkLoading = false
    state.perk = data
  },
  getPublishedLoading(state) {
    state.published = []
    state.publishedLoading = true
    state.publishedError = null
  },
  getPublishedSuccess(state, data) {
    state.publishedLoading = false
    state.published = data
    state.publishedError = null
  },
  getPublishedError(state, error) {
    state.published = []
    state.publishedLoading = false
    state.publishedError = error
  },
  getMineLoading(state) {
    state.mine = []
    state.mineLoading = true
    state.mineError = null
  },
  getMineSuccess(state, data) {
    state.mineLoading = false
    state.mine = data
    state.mineError = null
  },
  getMineError(state, error) {
    state.mine = []
    state.mineLoading = false
    state.mineError = error
  },
  userStateLoading(state) {
    state.userState = {}
    state.userStateLoading = true
    state.userStateError = null
  },
  userStateSuccess(state, data) {
    state.userStateLoading = false
    state.userState = data
    state.userStateError = null
  },
  userStateError(state, error) {
    state.userState = {}
    state.userStateLoading = false
    state.userStateError = error
  },
  subscribeLoading(state) {
    state.subscribeLoading = true
    state.subscribeError = null
  },
  subscribeSuccess(state) {
    state.subscribeLoading = false
    state.userState.subscribed = true
    state.subscribeError = null
  },
  subscribeError(state, error) {
    state.userState.subscribed = false
    state.subscribeLoading = false
    state.subscribeError = error
  },
  favoriteLoading(state) {
    state.favoriteLoading = true
    state.favoriteError = null
  },
  favoriteSuccess(state, { value, id }) {
    state.favoriteLoading = false
    state.favoriteError = null
    state.userState.favorite = value
    state.published = state.published.map(i => {
      if (i.id === id) {
        i.state = {
          ...i.state,
          favorite: value
        }
      }
      return i
    })
  },
  favoriteError(state, { error, id, value }) {
    state.published = state.published.map(i => {
      if (i.id === id) {
        i.state = {
          ...i.state,
          favorite: value
        }
      }
      return i
    })
    state.userState.favorite = value
    state.favoriteLoading = false
    state.favoriteError = error
  },
  statisticsLoading(state) {
    state.perkStats = {}
    state.statiscticsLoading = true
    state.statiscticsError = null
  },
  statisticsSuccess(state, stats) {
    state.perkStats = stats
    state.userState.subscribed = true
    state.statiscticsError = null
  },
  statisticsError(state, error) {
    state.perkStats = {}
    state.statiscticsLoading = false
    state.statiscticsError = error
  },
  rateLoading(state) {
    state.rateLoading = true
    state.rateError = null
  },
  rateSuccess(state) {
    state.rateLoading = false
    state.rateError = null
    state.userState.rated = true
    // TODO add comment in list
  },
  rateError(state, error) {
    state.rateLoading = false
    state.rateError = error
  },
  getRatingsLoading(state) {
    state.ratings = []
    state.getRatingsLoading = true
    state.getRatingsError = null
  },
  getRatingsSuccess(state, data) {
    state.ratings = data
    state.getRatingsLoading = false
    state.getRatingsError = null
  },
  getRatingsError(state, error) {
    state.getRatingsLoading = false
    state.getRatingsError = error
  },
  renewPerkLoading(state) {
    state.renewPerkLoading = true
    state.renewPerkError = null
  },
  renewPerkSuccess(state, data) {
    state.perk.expires_at = data.expires_at
    state.renewPerkLoading = false
    state.renewPerkError = null
  },
  renewPerkError(state, error) {
    state.renewPerkLoading = false
    state.renewPerkError = error
  }
}
