import api from '@/services/api'
import { ToastProgrammatic as Toast } from 'buefy'

export default {
  async getPerk({ commit }, id) {
    commit('perkLoading')
    try {
      const response = await api.get(`/api/perks/${id}`)
      commit('perkSuccess', response.data.data)
      commit('userStateSuccess', response.data.data.state)
      return true
    } catch (e) {
      commit('perkError', e.response.data)
      commit('userStateError', e.response.data)
      return false
    }
  },

  async getPublished({ commit }) {
    commit('getPublishedLoading')
    try {
      const response = await api.get('/api/perks/published')
      commit('getPublishedSuccess', response.data.data)
    } catch (e) {
      commit('getPublishedError', e.response.data)
    }
  },

  async getMine({ commit }) {
    commit('getMineLoading')
    try {
      const response = await api.get('/api/me/perks')
      commit('getMineSuccess', response.data.data)
    } catch (e) {
      commit('getMineError', e.response.data)
    }
  },

  async getUserState({ commit }, id) {
    commit('userStateLoading')
    try {
      const response = await api.get('/api/me/perks/' + id)
      commit('userStateSuccess', response.data)
    } catch (e) {
      commit('userStateError', e.response.data)
    }
  },

  async favorite({ commit }, { id, state }) {
    commit('favoriteLoading')
    try {
      const response = await api.put('/api/perks/' + id + '/favorite', {
        state: state
      })
      commit('favoriteSuccess', { value: state, id })
      return response
    } catch (e) {
      commit('favoriteError', {
        error: e.response.data,
        value: !state,
        id
      })
      Toast.open({
        message: e.response.data,
        type: 'is-danger',
        duration: 2000
      })
    }
  },

  async subscribe({ commit }, id) {
    commit('subscribeLoading')
    try {
      await api.post('/api/perks/' + id + '/subscribe')
      commit('subscribeSuccess')
    } catch (e) {
      commit('subscribeError', e.response.data)
    }
  },
  async getStatistics({ commit }, data) {
    commit('statisticsLoading')
    try {
      const response = await api.get(
        '/api/perks/' + data.perkId + '/statistics'
      )
      commit('statisticsSuccess', response.data.data)
    } catch (e) {
      commit('statisticsError', e.response.data)
    }
  },
  async rate({ commit }, { id, rating, comment, title }) {
    commit('rateLoading')
    try {
      await api.post('/api/perks/' + id + '/rating', {
        rating,
        comment,
        title
      })
      commit('rateSuccess')
      Toast.open({
        type: 'is-success',
        message: 'Your rate has been sent with success !',
        duration: 5000
      })
    } catch (e) {
      commit('rateError', e.response.data)
    }
  },

  async getRatings({ commit }, { id }) {
    commit('getRatingsLoading')
    try {
      const response = await api.get('/api/perks/' + id + '/ratings')
      commit('getRatingsSuccess', response.data.data)
    } catch (e) {
      commit('getRatingsError', e.response.data)
    }
  },

  async renewPerk({ commit }, { id, expires_in }) {
    commit('renewPerkLoading')
    try {
      const response = await api.post('/api/perks/' + id + '/renew', {
        expires_in
      })
      commit('renewPerkSuccess', response.data.data)
      return { code: 'success', message: response.data }
    } catch (e) {
      commit('renewPerkError', e.response.data)
      return { code: 'danger', message: e.response.data }
    }
  }
}
