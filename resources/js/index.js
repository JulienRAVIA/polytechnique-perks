import Vue from 'vue'

// App root component
import App from './App.vue'

// Layouts
import Default from '@/layouts/Default.vue'
import Empty from '@/layouts/Empty.vue'

Vue.component('default-layout', Default)
Vue.component('empty-layout', Empty)

// Vue Router
import router from './router'
import store from './store'

import Buefy from 'buefy'
Vue.use(Buefy, {
  defaultIconPack: 'icon'
})

import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
import '../sass/element_variable.scss'
import locale from 'element-ui/lib/locale/lang/en'
Vue.use(ElementUI, { locale })

Vue.use(require('vue-faker'))

// Filters
import filters from './filters'
filters.forEach(f => {
  Vue.filter(f.name, f.filter)
})

import VueMq from 'vue-mq'

Vue.use(VueMq, {
  breakpoints: {
    sm: 475,
    md: 1250,
    lg: Infinity
  },
  defaultBreakpoint: 'sm'
})

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
