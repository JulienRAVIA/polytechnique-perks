import moment from 'moment'

export default {
  name: 'date',
  filter: value => {
    return moment(value).format('YYYY-MM-DD')
  }
}
