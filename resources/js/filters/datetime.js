import moment from 'moment'

export default {
  name: 'datetime',
  filter: value => {
    return moment(value).format('YYYY-MM-DD HH:mm')
  }
}
