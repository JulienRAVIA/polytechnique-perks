import axios from 'axios'
import { ToastProgrammatic as Toast } from 'buefy'

const instance = axios.create({
  withCredentials: true
})
import router from '@/router'
import store from '@/store'

instance.interceptors.response.use(
  function(response) {
    return response
  },
  function(error) {
    if (
      error &&
      error.response &&
      error.response.status !== 401 &&
      error.response.data &&
      error.response.data.message
    ) {
      Toast.open({
        message: error.response.data.message,
        type: 'is-danger'
      })
    }
    if (axios.isCancel(error)) {
      return Promise.reject(error)
    }
    const { response } = error
    const path = response.config.url.replace(response.config.baseURL, '')

    /** @todo change **/
    if (path !== '/api/me') {
      console.log(
        response.data && response.data.message
          ? response.data.message
          : "Une erreur innatendue s'est produite"
      )
    }

    // Global redirection on 401
    if (response.status === 401) {
      store.commit('auth/logoutSuccess')

      if (path !== '/api/me') {
        router.push({ name: 'login' })
      }
    }
    return Promise.reject(error)
  }
)

export function errorMessage(error, key) {
  return error !== null &&
    error.errors !== undefined &&
    Object.prototype.hasOwnProperty.call(error.errors, key)
    ? error.errors[key].flat().join('\n')
    : null
}

export function extractErrorMessagesAsArray(error) {
  const errorsData = error.response && error.response && error.response.errors
  let errorMessages = []
  if (errorsData) {
    Object.values(errorsData).forEach(fieldError => {
      fieldError.forEach(errorMessage => {
        errorMessages.push(errorMessage)
      })
    })
  }

  return errorMessages
}

export default instance
