import Vue from 'vue'
import Router from 'vue-router'
import store from '@/store'

import Home from '@/screens/Home.vue'
import Login from '@/screens/Auth/Login.vue'
import Register from '@/screens/Auth/Register.vue'
import Profile from '@/screens/Me/Profile.vue'
import Security from '@/screens/Me/Security.vue'
import Privacy from '@/screens/Me/Privacy.vue'
import Stats from '@/screens/Stats'
import Reset from '@/screens/Auth/Reset'
import ActivationPage from '@/screens/Auth/ActivationPage'
import AuthRefused from '@/screens/Auth/Refused'
import Admin from '@/layouts/Admin.vue'
import Me from '@/layouts/Me.vue'
import CreatePerk from '@/screens/CreatePerk.vue'
import PerkDetail from '@/screens/Perk/Detail.vue'
import PerkMine from '@/screens/Perk/Mine.vue'
import UsersList from '@/screens/admin/UsersList.vue'
import PerksList from '@/screens/admin/PerksList.vue'

Vue.use(Router)

const router = new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home,
      beforeEnter: (to, from, next) => {
        if (store.getters['auth/canProposePerk']) {
          return next({ name: 'mine' })
        } else {
          return next()
        }
      }
    },
    {
      path: '/login',
      name: 'login',
      component: Login,
      meta: { layout: 'empty', guest: true }
    },
    {
      path: '/register',
      name: 'register',
      component: Register,
      meta: { layout: 'empty', guest: true }
    },
    {
      path: '/password/reset',
      name: 'reset_password',
      component: Reset,
      meta: { layout: 'empty', guest: true }
    },
    {
      path: '/activation',
      name: 'activation_page',
      component: ActivationPage,
      meta: { layout: 'empty', guest: true }
    },
    {
      path: '/refused',
      name: 'login_refused',
      component: AuthRefused,
      meta: { layout: 'empty', guest: true }
    },
    {
      path: '/create-perk',
      name: 'createPerk',
      component: CreatePerk,
      beforeEnter: (to, from, next) => {
        if (
          store.getters['auth/canProposePerk'] ||
          store.getters['auth/isAdmin']
        ) {
          return next()
        } else {
          return next({ name: 'home' })
        }
      }
    },
    {
      path: '/perk/:title',
      name: 'perkDetail',
      component: PerkDetail
    },
    {
      path: '/perk/:id/stats',
      name: 'perkStats',
      component: Stats
    },
    {
      path: '/me/perks',
      name: 'mine',
      component: PerkMine
    },
    {
      path: '/admin',
      component: Admin,
      name: 'admin',
      redirect: '/admin/users',
      beforeEnter: (to, from, next) => {
        if (store.getters['auth/isAdmin']) {
          return next()
        } else {
          return next({ name: 'home' })
        }
      },
      children: [
        {
          path: 'users',
          name: 'usersList',
          component: UsersList
        },
        {
          path: 'perks',
          name: 'perksList',
          component: PerksList
        }
      ]
    },
    {
      path: '/me',
      component: Me,
      name: 'me',
      redirect: '/me/profile',
      children: [
        {
          path: 'profile',
          component: Profile,
          name: 'profile'
        },
        {
          path: 'security',
          component: Security,
          name: 'security'
        },
        {
          path: 'privacy',
          component: Privacy,
          name: 'privacy'
        }
      ]
    }
  ]
})

router.beforeEach(async (to, from, next) => {
  store.dispatch('common/setGlobalLoading', true)
  await store.dispatch('auth/me')

  if (to.meta.guest === true) {
    if (store.getters['auth/isLoggedIn']) {
      return next({ name: 'home' })
    }
  } else {
    if (!store.getters['auth/isLoggedIn']) {
      return next('login')
    }
  }

  return next()
})

router.afterEach(() => {
  store.dispatch('common/setGlobalLoading', false)
  store.dispatch('common/setBooting', false)
  store.dispatch('common/resetHeader')
})

export default router
