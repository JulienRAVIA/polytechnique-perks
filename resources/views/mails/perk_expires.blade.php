<div>
    Hello {{ $user->email }},

    {{ count($perks) }} perks you have created expires in {{ $delay }} days.
    Here's the list :

    <ul>
        @foreach ($perks as $perk)
            <li>{{ $perk->title }}</li>
        @endforeach
    </ul>
</div>
