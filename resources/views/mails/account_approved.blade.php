<div>
    Dear {{ $user->first_name  }}, <br>

    We are happy to announce that your account has been validated and that you are now part of the X-PERKS community !
    <br>
    You can now connect to {{ url('/') }} where you can enter your email and password to access all the great deals from our partners.
    <br>

    Kind regards,
</div>
