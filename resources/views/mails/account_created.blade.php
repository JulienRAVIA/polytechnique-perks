<div>
    Dear {{ $user->firstName }}, <br>

    Thank you for registering to the X-Perks platform !

    We have confirmed that your account has been created and that we have emailed the administrator. <br>

    You will receive an email as soon as your account has been confirmed. <br>
    Kind regards,
</div>
