<div>
    Dear {{ $user->firstName }}, <br>

    We are sorry but your X-PERKS account has been refused by the administrator.
    It seems that you are not a member of X-UP, X-TECH or an Ecole polytechnique alumni and entrepreneur. <br>

    If you have any questions you can send an email to <a href="mailto:contact-dei@polytechnique.fr">contact-dei@polytechnique.fr</a>. <br>

    Kind regards,
</div>
