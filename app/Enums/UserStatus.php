<?php

namespace App\Enums;

use BenSampo\Enum\Enum;

/**
 * @method static static PENDING()
 * @method static static VALIDATED()
 * @method static static REFUSED()
 */
final class UserStatus extends Enum
{
    const PENDING =   'pending';
    const VALIDATED = 'validated';
    const REFUSED =   'refused';

    public static function getDescription($value): string
    {
        if ($value === self::PENDING) {
            return 'Waiting for approval';
        }

        return parent::getDescription($value);
    }
}
