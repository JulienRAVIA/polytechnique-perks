<?php

namespace App\Enums;

use BenSampo\Enum\Enum;

final class Permission extends Enum
{
    const PROPOSE_PERK = 'propose perk';
    const SHOW_PERKS = 'show perks';
    const MANAGE_PLATFORM = 'manage platform';
    const BE_EXPORTED = 'be exported';
}
