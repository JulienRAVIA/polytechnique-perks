<?php

namespace App\Enums;

use BenSampo\Enum\Enum;

/**
 * @method static static PENDING()
 * @method static static DRAFT()
 * @method static static REFUSED()
 * @method static static PUBLISHED()
 */
final class PerkStatus extends Enum
{
    const PENDING   = 'pending';
    const DRAFT     = 'draft';
    const REFUSED   = 'refused';
    const PUBLISHED = 'published';
    const EXPIRED   = 'expired';

    public static function getDescription($value): string
    {
        if ($value === self::PENDING) {
            return 'Waiting for approval';
        }

        return parent::getDescription($value);
    }
}
