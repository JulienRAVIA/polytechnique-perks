<?php

namespace App\Exports;

use App\Enums\Permission;
use App\Http\Resources\UserResource;
use App\Models\User;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Excel;

class UsersExport implements FromCollection
{
    use Exportable;

    /**
     * Optional Writer Type
     */
    private $writerType = Excel::CSV;

    /**
     * Optional headers
     */
    private $headers = [
        'Content-Type' => 'text/csv',
    ];

    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return User::permission(Permission::BE_EXPORTED)->get();
    }
}
