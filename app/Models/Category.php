<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class Category extends Model
{
    /**
     * Perks of the category
     *
     * @return BelongsToMany
     */
    public function perks(): BelongsToMany
    {
        return $this->belongsToMany(Perk::class, 'perk_has_categories', 'category_id ');
    }
}
