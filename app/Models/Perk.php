<?php

namespace App\Models;

use App\Enums\PerkStatus;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\SoftDeletes;

class Perk extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'title',
        'tagline',
        'description',
        'subscription_conditions',
        'contact_email_address',
        'website_url',
        'status',
        'is_published',
        'user_id',
        'offer',
        'expires_at'
    ];

    protected $appends = ['rate'];

    // SCOPES

    /**
     * Scope for filtering perks published
     *
     * @param Builder $query
     * @return mixed
     */
    public function scopePublished($query)
    {
        return $query->where('status', '=', PerkStatus::PUBLISHED);
    }

    /**
     * Scope for filtering perks activated
     *
     * @param Builder $query
     * @param string $value
     * @return mixed
     */
    public function scopeStatus($query, string $value = PerkStatus::PUBLISHED)
    {
        return $query->where('status', $value);
    }

    /**
     * Scope for filtering perks activated
     *
     * @param Builder $query
     * @return mixed
     */
    public function scopeExpired($query)
    {
        return $query->where('status', '=', PerkStatus::EXPIRED);
    }

    /**
     * Get rate.
     *
     * @return float
     */
    public function getRateAttribute()
    {
        return round($this->ratings()->average('note'), 1, PHP_ROUND_HALF_DOWN);
    }

    // RELATIONSHIPS

    /**
     * Categories of the perk
     *
     * @return BelongsToMany
     */
    public function categories(): BelongsToMany
    {
        return $this->belongsToMany(Category::class, 'perk_has_categories', 'perk_id');
    }

    /**
     * Author of the perk
     *
     * @return BelongsTo
     */
    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    /**
     * Users that have the perk as favorite
     *
     * @return BelongsToMany
     */
    public function favorites()
    {
        return $this->belongsToMany(User::class, 'favorites');
    }

    /**
     * Users that have subscribed to the perk
     *
     * @return BelongsToMany
     */
    public function subscriptions()
    {
        return $this->belongsToMany(User::class, 'subscriptions');
    }

    /**
     * Ratings of the perk
     *
     * @return BelongsToMany
     */
    public function ratings()
    {
        return $this->belongsToMany(User::class, 'ratings')->withPivot('title', 'comment', 'note');
    }

    /**
     * Return default status
     *
     * @return string
     */
    public static function getDefaultStatus()
    {
        return PerkStatus::PENDING;
    }
}
