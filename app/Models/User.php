<?php

namespace App\Models;

use App\Enums\UserStatus;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Storage;
use Spatie\Permission\Traits\HasRoles;

class User extends Authenticatable
{
    use Notifiable, HasRoles, SoftDeletes;

    protected $guard_name = 'api';

    const LOGO_FOLDER = 'logo';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'firstname', 'lastname', 'email', 'password', 'company_logo', 'company_name', 'remember_token', 'status'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
        'status' => UserStatus::class
    ];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = ['logo'];

    /**
     * Perks created by the user
     *
     * @return HasMany
     */
    public function perks(): HasMany
    {
        return $this->hasMany(Perk::class, 'user_id');
    }

    /**
     * Favorites perks of the user
     *
     * @return BelongsToMany
     */
    public function favorites(): BelongsToMany
    {
        return $this->belongsToMany(Perk::class, 'favorites');
    }

    /**
     * Subscribed perks of the user
     *
     * @return BelongsToMany
     */
    public function subscriptions()
    {
        return $this->belongsToMany(Perk::class, 'subscriptions');
    }

    /**
     * Rating writted by the user
     *
     * @return BelongsToMany
     */
    public function ratings(): BelongsToMany
    {
        return $this->belongsToMany(Perk::class, 'ratings');
    }

    /**
     * @return string
     */
    public function getRole(): string
    {
        return $this->getRoleNames()->first();
    }

    /**
     * Get default status
     *
     * @return string
     */
    public static function getDefaultStatus()
    {
        return UserStatus::PENDING;
    }

    /**
     * @return BelongsToMany
     */
    public function collaborations()
    {
        return $this->belongsToMany(Collaboration::class);
    }

    /**
     * @return string|null
     */
    public function getLogoAttribute()
    {
        return $this->company_logo ? Storage::disk()->url($this->company_logo) : null;
    }
}
