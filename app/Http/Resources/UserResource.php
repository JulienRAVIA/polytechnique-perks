<?php

namespace App\Http\Resources;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Resources\Json\JsonResource;

class UserResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'lastname' => $this->lastname,
            'firstname' => $this->firstname,
            'email' => $this->email,
            'roles' => RoleResource::collection($this->roles),
            'role' => $this->getRoleNames()->first(),
            'permissions' => PermissionResource::collection($this->getPermissionsViaRoles()),
            'company' => $this->company_name,
            'status' => $this->status,
            'is_deleted' => ($this->deleted_at === null) ? false : true
        ];
    }
}
