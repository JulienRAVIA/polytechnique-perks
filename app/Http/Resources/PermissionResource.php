<?php

namespace App\Http\Resources;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Resources\Json\JsonResource;
use Spatie\Permission\Models\Role;

class PermissionResource extends JsonResource
{
    public function toArray($request)
    {
        return $this->name;
    }
}
