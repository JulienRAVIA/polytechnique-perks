<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class PerkStatisticsResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'clicks' => $this->clicks_number,
            'ratings' => ($this->ratings) ? $this->ratings->count() : 0,
            'subscriptions' => ($this->subscriptions) ? $this->subscriptions->count() : 0,
            'favorites' => ($this->favorites) ? $this->favorites->count() : 0,
        ];
    }
}
