<?php

namespace App\Http\Resources;

use App\Enums\Permission;
use App\Models\Perk;
use Illuminate\Http\Resources\Json\JsonResource;

class UserPerksResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'subscriptions' => $this->when($this->can(Permission::SHOW_PERKS), PerkResource::collection(
                $this->subscriptions()->published()->get()
            )),
            'favorites' => $this->when($this->can(Permission::SHOW_PERKS), PerkResource::collection(
                $this->favorites()->published()->get()
            )),
            'owning' => $this->when($this->hasPermissionTo(Permission::PROPOSE_PERK), PerkResource::collection(
                $this->perks
            )),
        ];
    }
}
