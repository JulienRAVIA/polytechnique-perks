<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Auth;

class PerkStateResource extends JsonResource
{


    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'owner'         => $this->user_id === Auth::id(),
            'subscribed'    => Auth::user() !== null ?
                Auth::user()->subscriptions()->where('perk_id', $this->id)->exists()
                : false,
            'rated'         => Auth::user() !== null ?
                Auth::user()->ratings()->where('perk_id', $this->id)->exists()
                : false,
            'favorite'     => Auth::user() !== null ?
                Auth::user()->favorites()->where('perk_id', $this->id)->exists()
                : false,
        ];
    }
}
