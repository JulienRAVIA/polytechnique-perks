<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class RatingResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'title'     => $this->pivot->title,
            'comment'   => $this->pivot->comment,
            'rating'    => $this->pivot->note,
            'author'    => $this->firstname . ' ' . $this->lastname
        ];
    }
}
