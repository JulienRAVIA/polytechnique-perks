<?php

namespace App\Http\Resources;

use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class PerkResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'                => $this->id,
            'title'             => $this->title,
            'slug'              => Str::slug($this->title),
            'tagline'           => $this->tagline,
            'contact_email_address' => $this->contact_email_address,
            'website_url'       => $this->website_url,
            'author'            => $this->user,
            'description'       => $this->description,
            'subscription_conditions' => $this->subscription_conditions,
            'offer'             => $this->offer,
            'rate'              => $this->rate,
            'is_deleted'        => ($this->deleted_at !== null) ? true : false,
            'expires_at'        => $this->expires_at,
            'categories'        => CategoryResource::collection($this->categories),
            'category'          => new CategoryResource($this->categories->first()),
            'status'            => $this->status,
            'state'             => new PerkStateResource($this)
        ];
    }
}
