<?php

namespace App\Http\Controllers\Auth;

use App\Enums\Permission;
use App\Enums\UserStatus;
use App\Http\Resources\UserResource;
use App\Jobs\SendConfirmationRegistrationMail;
use App\Mail\RegistrationConfirmation;
use App\Models\Collaboration;
use App\Models\User;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\Controller;
use Illuminate\Auth\Events\Registered;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Spatie\Permission\Models\Role;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        $rules = [
            'email'         => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password'      => ['required', 'string', 'min:6'],
            'firstname'     => ['required', 'string', 'max:255'],
            'lastname'      => ['required', 'string', 'max:255'],
            'company_name'  => ['required', 'string', 'max:255'],
            'propose_perk'  => ['required', 'string', 'in:yes,no'],
            'conditions'    => ['required', 'accepted'],
            'collaboration' => ['required_if:propose_perk,no', 'exists:collaborations,id', 'nullable'],
            'logo'          => ['required_if:propose_perk,yes', 'image', 'nullable'],
        ];

        return Validator::make($data, $rules);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\Models\User
     */
    protected function create(array $data): User
    {
        return DB::transaction(function () use ($data) {
            $user = User::create([
                'email'         => $data['email'],
                'password'      => Hash::make($data['password']),
                'firstname'     => $data['firstname'],
                'lastname'      => $data['lastname'],
                'company_name'  => $data['company_name'],
                'status'        => UserStatus::PENDING
            ]);

            if ($data['propose_perk'] === 'no') {
                $user->collaborations()->attach($data['collaboration']);
            } else {
                $path = $data['logo']->store(User::LOGO_FOLDER);
                $user->fill(['company_logo' => $path])->save();
            }

            // @todo hard-coded role
            $user->assignRole($data['propose_perk'] === 'yes' ? 'company' : 'startup');

            return $user;
        });
    }

    /**
     * The user has been registered.
     *
     * @param Request $request
     * @param mixed $user
     * @return UserResource
     */
    protected function registered(Request $request, $user)
    {
        Mail::send(new RegistrationConfirmation($user));

        event(new Registered($user));

        return new UserResource($user);
    }
}
