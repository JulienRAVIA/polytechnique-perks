<?php

namespace App\Http\Controllers\Auth;

use App\Enums\UserStatus;
use App\Http\Controllers\Controller;
use App\Http\Resources\UserResource;
use BenSampo\Enum\Enum;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\ValidationException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;
use Symfony\Component\HttpKernel\Exception\UnprocessableEntityHttpException;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * The user has been authenticated.
     *
     * @param Request $request
     * @param mixed $user
     * @return mixed
     *
     * @throws ValidationException
     */
    protected function authenticated(Request $request, $user)
    {
        switch ($user->status) {
            case UserStatus::PENDING:
                Auth::logout();
                throw new UnauthorizedHttpException('Session', 'Register pending');
            case UserStatus::REFUSED:
                Auth::logout();
                return abort(403, 'Registration refused');
            case UserStatus::VALIDATED:
                return new UserResource($user);
            default:
                Auth::logout();
                throw ValidationException::withMessages([
                    $this->username() => [trans('auth.failed')],
                ]);
        }
    }

    /**
     * The user has logged out of the application.
     *
     * @param Request $request
     * @return mixed
     */
    protected function loggedOut(Request $request)
    {
        return response()->json(['message' => 'You have been successfully disconnected.'], 200);
    }
}
