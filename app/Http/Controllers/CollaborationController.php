<?php

namespace App\Http\Controllers;

use App\Models\Collaboration;

/**
 * Class CollaborationController
 * @package App\Http\Controllers
 */
class CollaborationController extends Controller
{
    public function all()
    {
        return Collaboration::all();
    }
}
