<?php

namespace App\Http\Controllers;

use App\Enums\PerkStatus;
use App\Enums\Permission;
use App\Http\Requests\ChangeStatusRequest;
use App\Http\Requests\CreatePerkRequest;
use App\Http\Requests\RatePerkRequest;
use App\Http\Requests\RenewPerkRequest;
use App\Http\Requests\SetFavoriteRequest;
use App\Http\Requests\UpdatePerkRequest;
use App\Http\Resources\PerkResource;
use App\Http\Resources\PerkStatisticsResource;
use App\Http\Resources\RatingResource;
use App\Models\Perk;
use Carbon\Carbon;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;

/**
 * Class PerkController
 * @package App\Http\Controllers
 *
 * @todo refactoring global pour éviter de faire des insert en base directement
 * @todo refactoring globale pour retourner des ressources
 */
class PerkController extends Controller
{
    /**
     * List all perks
     *
     * @return AnonymousResourceCollection
     */
    public function list()
    {
        return PerkResource::collection(Perk::paginate(15));
    }

    /**
     * Get perk
     *
     * @param Perk $perk
     * @return PerkResource|NotFoundHttpException|AccessDeniedHttpException
     */
    public function get(Perk $perk)
    {
        $user = Auth::user();

        if ($user === null) {
            return new AccessDeniedHttpException("You're not allowed to see this", null, 403);
        }

        if ($perk->user_id !== $user->id) {
            if ($user->can(Permission::SHOW_PERKS)) {
                if ($perk->status !== PerkStatus::PUBLISHED) {
                    return new NotFoundHttpException('This perks does not exist', null, 404);
                }
                $perk->clicks_number += 1;
            } elseif ($user->can(Permission::PROPOSE_PERK)) {
                return new NotFoundHttpException("You're not allowed to see this perk", null, 403);
            }
        }
        return new PerkResource($perk);
    }

    /**
     * Delete perk
     *
     * @param Perk $perk
     * @return PerkResource
     * @throws \Exception
     */
    public function delete(Perk $perk)
    {
        $perk->delete();
        return new PerkResource($perk);
    }

    /**
     * Create perk
     *
     * @param createPerkRequest $request
     * @return PerkResource
     */
    public function create(CreatePerkRequest $request)
    {
        $newPerk = DB::transaction(function () use ($request) {
            $now = Carbon::now();
            $user = $request->user();
            
            $perk = Perk::create([
                'title' => $request->input('title'),
                'tagline' => implode(', ', $request->get('tagline')),
                'description' => $request->get('description'),
                'subscription_conditions' => $request->get('subscription_conditions'),
                'contact_email_address' => $request->get('contact_email_address'),
                'website_url' => $request->get('website_url'),
                'status' => $request->get('publish') ? PerkStatus::PENDING : PerkStatus::DRAFT,
                'user_id' => ($user->can(Permission::MANAGE_PLATFORM)) ? $request->get('author') : $user->id,
                'offer' => $request->get('offer'),
                'expires_at' => $now->setTime(23, 59, 0)->addMonths(
                    $request->get('expires_in', 12)
                )
            ]);

            $categories = $request->get('categories');
            $categories = array_column($categories, 'id');
            $perk->categories()->sync($categories);

            return $perk;
        });

        return new PerkResource($newPerk);
    }

    /**
     * Update perk
     *
     * @param UpdatePerkRequest $request
     * @param Perk $perk
     * @return PerkResource
     */
    public function update(UpdatePerkRequest $request, Perk $perk)
    {
        $user = Auth::user();

        // A company can't update perks from other companies
        if ($user && $perk->user_id !== $user->id && $user->hasPermissionTo(Permission::PROPOSE_PERK)) {
            throw new UnauthorizedHttpException('session', "You can't update perks that are not yours.");
        }

        // A company can't update published perks
        if ($user && $perk->status === PerkStatus::PUBLISHED && $user->hasPermissionTo(Permission::PROPOSE_PERK)) {
            throw new UnauthorizedHttpException(
                'session',
                "You can't update published perks. Contact an administrator to get some help."
            );
        }

        // A company can't update expired perks
        if ($perk->status === PerkStatus::EXPIRED) {
            throw new UnauthorizedHttpException(
                'session',
                "You can't update expired perks. Please, try creating a new one."
            );
        }

        $updatedPerk = DB::transaction(function () use ($request, $perk) {
            $perk->update([
                'title' => $request->input('title'),
                'tagline' => implode(', ', $request->get('tagline')),
                'description' => $request->get('description'),
                'subscription_conditions' => $request->get('subscription_conditions'),
                'contact_email_address' => $request->get('contact_email_address'),
                'website_url' => $request->get('website_url'),
                'status' => $request->get('publish') ? PerkStatus::PENDING : PerkStatus::DRAFT,
                'user_id' => $request->get('author'),
                'offer' => $request->get('offer'),
            ]);

            return $perk;
        });

        return new PerkResource($updatedPerk);
    }

    /**
     * @param Perk $perk
     * @param ChangeStatusRequest $request
     * @return JsonResponse
     */
    public function changeStatus(Perk $perk, ChangeStatusRequest $request)
    {
        if ($request->input('status')) {
            $status = PerkStatus::PUBLISHED;
        } else {
            $status = PerkStatus::REFUSED;
        }
        $perk->update([
            'status' => $status
        ]);
        return response()->json([], 204);
    }

    /**
     * List perks (favorites, owning, subscriptions)
     *
     * @return AnonymousResourceCollection
     */
    public function getPublished()
    {
        // Get only my perks
        if (Auth::user() !== null && Auth::user()->can(Permission::PROPOSE_PERK)) {
            $perks = Auth::user()->perks();
        } else {
            $perks = Perk::published()->paginate(15);
        }

        return PerkResource::collection($perks);
    }

    /**
     * @param Perk $perk
     * @param SetFavoriteRequest $request
     * @return JsonResponse
     */
    public function setFavorite(Perk $perk, SetFavoriteRequest $request)
    {
        $favorite = DB::table('favorites')->select('id')
            ->where([['user_id', '=', Auth::id()], ['perk_id', '=', $perk->id]])->first();

        if ($favorite && $request->input('state') == false) {
            DB::table('favorites')->delete($favorite->id);
            return response()->json('Favorite perk removed.', 200);
        } elseif ($favorite === null && $request->input('state') == true) {
            DB::table('favorites')->insert(['user_id' => Auth::id(), 'perk_id' => $perk->id]);
            return response()->json('Favorite perk added.', 200);
        }

        return response()->json('Error while managing perk.', 422);
    }

    /**
     * @param Perk $perk
     * @return JsonResponse
     */
    public function subscribe(Perk $perk)
    {
        $subscribed = DB::table('subscriptions')->select('id')
            ->where([['user_id', '=', Auth::id()], ['perk_id', '=', $perk->id]])->first();

        if ($subscribed !== null) {
            return response()->json([422, 'You already subscribed to this perk.']);
        } elseif ($subscribed === null && $perk->status === PerkStatus::PUBLISHED) {
            DB::table('subscriptions')->insert(['user_id' => Auth::id(), 'perk_id' => $perk->id]);
            return response()->json([200, 'Successfully subscribed to the perk.']);
        }
        return response()->json([422, 'Error while subscribing to the perk.']);
    }

    /**
     * @param Perk $perk
     * @param RatePerkRequest $request
     * @return JsonResponse
     */
    public function addRating(Perk $perk, RatePerkRequest $request)
    {
        $subscribedPerk = DB::table('subscriptions')->select('perk_id')
            ->where([['user_id', '=', Auth::id()], ['perk_id', $perk->id]])->first();

        if ($subscribedPerk === null) {
            return response()->json([422, "You didn't subscribed to the perk."]);
        }

        $rating = DB::table('ratings')
            ->select('id')
            ->where([['user_id', '=', Auth::id()], ['perk_id', '=', $perk->id]])->first();

        if ($rating !== null) {
            return response()->json([422, 'You already rated this perk.']);
        } elseif ($rating === null) {
            DB::table('ratings')
                ->insert([
                    'user_id' => Auth::id(),
                    'perk_id' => $perk->id,
                    'note' => $request->input('rating'),
                    'title' => $request->input('title'),
                    'comment' => $request->input('comment')
                ]);
            return response()->json([200, 'Perk rate has been saved.']);
        }
        return response()->json([422, 'Error while rating the the perk.']);
    }

    /**
     * @param Perk $perk
     * @return AnonymousResourceCollection
     */
    public function getRating(Perk $perk)
    {
        return RatingResource::collection($perk->ratings()->withPivot('title', 'comment', 'note')->get());
    }

    /**
     * @param Perk $perk
     * @return PerkStatisticsResource
     */
    public function getStatistics(Perk $perk)
    {
        return new PerkStatisticsResource($perk);
    }

    /**
     * @param Perk $perk
     * @param RenewPerkRequest $request
     * @return PerkResource
     */
    public function renew(Perk $perk, RenewPerkRequest $request)
    {
        $user = Auth::user();

        if ($user && $user->id !== $perk->user_id && !$user->hasPermissionTo(Permission::MANAGE_PLATFORM)) {
            throw new UnauthorizedHttpException('session', "You can't renew perks that are not yours.");
        }

        if ($perk->status !== PerkStatus::PUBLISHED) {
            throw new UnauthorizedHttpException('session', "You can't renew a perk that is not published.");
        }

        $renewedPerk = DB::transaction(function () use ($request, $perk) {
            $perk->update([
                'expires_at' => Carbon::now()->setTime(23, 59, 0)->addMonths(
                    $request->get('expires_in', 12)
                )
            ]);

            return $perk;
        });

        return new PerkResource($renewedPerk);
    }
}
