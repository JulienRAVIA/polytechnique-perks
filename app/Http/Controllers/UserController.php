<?php

namespace App\Http\Controllers;

use App\Enums\Permission;
use App\Enums\UserStatus;
use App\Exports\UsersExport;
use App\Http\Requests\ChangeStatusRequest;
use App\Http\Resources\CompanyResource;
use App\Http\Resources\PerkStateResource;
use App\Http\Resources\UserPerksResource;
use App\Http\Resources\UserResource;
use App\Mail\RejectedAccount;
use App\Mail\ValidatedAccount;
use App\Models\Perk;
use App\Models\User;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Maatwebsite\Excel\Facades\Excel;
use Symfony\Component\HttpFoundation\BinaryFileResponse;

class UserController extends Controller
{
    /**
     * List users (even those deleted)
     *
     * @return AnonymousResourceCollection
     */
    public function list()
    {
        return UserResource::collection(
            User::withTrashed()->paginate(15)
        );
    }

    /**
     * List users (even those deleted)
     *
     * @param User $user
     * @return UserResource
     */
    public function get(User $user)
    {
        return new UserResource($user);
    }

    /**
     * Delete user
     *
     * @param User $user
     * @return UserResource
     * @throws Exception
     */
    public function delete(User $user)
    {
        $user->delete();
        return new UserResource($user);
    }

    /**
     * Restore user
     *
     * @param User $user
     * @return UserResource
     */
    public function restore(User $user)
    {
        $user->restore();
        return new UserResource($user);
    }

    /**
     * @param User $user
     * @param ChangeStatusRequest $request
     * @return JsonResponse
     */
    public function changeStatus(User $user, ChangeStatusRequest $request)
    {
        if ($request->input('status')) {
            $status = UserStatus::VALIDATED;
            Mail::send(new ValidatedAccount($user));
        } else {
            $status = UserStatus::REFUSED;
            Mail::send(new RejectedAccount($user));
        }
        $user->update([
            'status' => $status
        ]);
        return response()->json([], 204);
    }

    /**
     * List perks (favorites, owning, subscriptions)
     *
     * @return UserPerksResource
     */
    public function listPerks()
    {
        $user = auth()->user();

        return new UserPerksResource($user);
    }

    /**
     * @param Perk $perk
     * @return PerkStateResource
     */
    public function perk(Perk $perk)
    {
        return new PerkStateResource($perk);
    }

    /**
     * @todo Send mail to the user with attachment ?
     *
     * @return BinaryFileResponse
     */
    public function export()
    {
        return Excel::download(new UsersExport(), 'users.csv');
    }

    /**
     * List all companies
     *
     * @return AnonymousResourceCollection
     */
    public function listCompanies()
    {
        $companies = User::permission(Permission::PROPOSE_PERK)->get();

        return CompanyResource::collection($companies);
    }
}
