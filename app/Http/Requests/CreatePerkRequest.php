<?php

namespace App\Http\Requests;

use App\Enums\Permission;
use App\Models\User;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Validator;

class CreatePerkRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'title' => 'required|string',
            'tagline' => 'required|array',
            'tagline.*' => 'string',
            'description' => 'required|string',
            'subscription_conditions' => 'required|string',
            'website_url' => 'required|string|url',
            'publish' => 'required|boolean',
            'categories' => 'required|array',
            'contact_email_address' => 'required|email',
            'offer' => 'required|string',
            'expires_in' => 'required|numeric|between:1,12',
        ];

        if ($this->user()->can(Permission::MANAGE_PLATFORM)) {
            $rules['author'] = 'required|numeric|exists:users,id';
        }

        return $rules;
    }

    /**
     * @param Validator $validator
     */
    public function withValidator($validator)
    {
        $validator->after(function ($validator) {
            // we check the author selected is allowed to create a perk
            if ($this->exists('author') && is_int($this->author)) {
                $user = User::find($this->author);
                if ($user !== null && !$user->can(Permission::PROPOSE_PERK)) {
                    $validator->errors()->add('author', 'This user is not a company');
                }
            }
        });
    }
}
