<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RatePerkRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'rating'    => 'required|integer|min:1|max:5',
            'comment'   => 'required|string',
            'title'     => 'required|string',
        ];
    }
}
