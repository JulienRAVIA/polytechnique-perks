<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdatePerkRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required|string',
            'tagline' => 'required|array',
            'tagline.*' => 'string',
            'description' => 'required|string',
            'subscription_conditions' => 'required|string',
            'website_url' => 'required|string|url',
            'publish' => 'required|boolean',
            'categories' => 'required|array',
            'contact_email_address' => 'required|email',
            'offer' => 'required|string',
            'author' => 'required|numeric|exists:users,id',
        ];
    }
}
