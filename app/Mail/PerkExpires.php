<?php

namespace App\Mail;

use App\Models\Perk;
use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class PerkExpires extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * @var array
     */
    private $perks;

    /**
     * @var User
     */
    private $user;

    /**
     * Create a new message instance.
     *
     * @param User $user
     * @param array $perks
     * @param int $delay
     */
    public function __construct(User $user, array $perks, int $delay)
    {
        $this->subject(
            sprintf('%d perk(s) you have created expires in %d days', count($perks), $delay)
        );
        $this->perks = $perks;
        $this->delay = $delay;
        $this->user = $user;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('mails/perk_expires')
            ->with([
                'user' => $this->user,
                'delay' => $this->delay,
                'perks' => $this->perks
            ])
            ->to($this->user->email);
    }
}
