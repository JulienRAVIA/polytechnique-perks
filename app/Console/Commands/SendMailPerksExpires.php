<?php

namespace App\Console\Commands;

use App\Mail\PerkExpires;
use App\Models\Perk;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\Mail;

class SendMailPerksExpires extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'mail:send:perks-expires {delay=30}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description =
        'Send mail to user where their published and validated perks expires in a provided delay of days';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     */
    public function handle()
    {
        $delay = $this->argument('delay');
        if (!is_numeric($delay)) {
            throw new \InvalidArgumentException('Invalid delay');
        }
        $users = $this->sortPerksByUsers($this->getPerksExpiringInDays((int)$delay));

        foreach ($users as $user) {
            Mail::send(new PerkExpires($user['user'], $user['perks'], (int)$delay));
        }

        $this->info(
            sprintf('Mail has been sended to %d users', count($users))
        );
    }

    /**
     * Return collection of published and validated perks expiring in $delay days
     *
     * @param int $delay
     * @return Perk[]|Collection
     */
    public function getPerksExpiringInDays(int $delay = 30)
    {
        $minDate = Carbon::today()->addDays($delay);
        $maxDate = clone $minDate;
        $maxDate->addDay();

        $perks = Perk::all()
            ->whereBetween('expires_at', [
                $minDate->toDateTimeString(),
                $maxDate->toDateTimeString()
            ])
            ->where('is_published', true)
            ->where('is_validated', true)
        ;

        return $perks;
    }

    /**
     * Sort perks by user
     *
     * @param Collection $perks
     * @return array
     */
    public function sortPerksByUsers(Collection $perks)
    {
        $users = [];
        foreach ($perks as $perk) {
            if (!array_key_exists($perk->user->email, $users)) {
                $users[$perk->user->email]['user'] = $perk->user;
            }
            $users[$perk->user->email]['perks'][] = $perk;
        }

        return $users;
    }
}
