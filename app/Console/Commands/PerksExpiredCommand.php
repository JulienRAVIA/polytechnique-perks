<?php

namespace App\Console\Commands;

use App\Enums\PerkStatus;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class PerksExpiredCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'check:perks-expired';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Set status "expired" to expired perks';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $request = DB::table('perks')
            ->where('expires_at', '<=', Carbon::now())
            ->where('status', '=', PerkStatus::PUBLISHED)
            ->update(['status' => PerkStatus::EXPIRED])
        ;

        $this->info(sprintf('%s perks has been set as expired', $request));
    }
}
