<?php

use App\Enums\Permission;
use Illuminate\Support\Arr;
use App\Models\{ Perk, User };
use Illuminate\Database\Seeder;

class UsersAndPerksTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $company = User::role('company')->first();

        $company->perks()->saveMany(
            factory(Perk::class, rand(1, 3))->make()
        );

        factory(User::class, 50)
            ->create()
            ->each(function (User $user) {
                $user->assignRole(Arr::random(RoleAndPermissionsTableSeeder::$roles));

                if ($user->hasPermissionTo(Permission::PROPOSE_PERK)) {
                    $user->perks()->saveMany(
                        factory(Perk::class, rand(1, 3))->make()
                    );
                }
            });
    }
}
