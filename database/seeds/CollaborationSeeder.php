<?php

use App\Models\Collaboration;
use Illuminate\Database\Seeder;

class CollaborationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $collaborations = [
            'Incubator X-UP (current/alumni)',
            'Booster X-TECH (current/alumni)',
            'Graduated from Ecole polytechnique and Entrepreneur',
        ];

        foreach($collaborations as $collaboration) {
            Collaboration::create([
                'label' => $collaboration,
            ]);
        }
    }
}
