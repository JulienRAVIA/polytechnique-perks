<?php

use App\Models\{ Category, Perk };
use Faker\Factory;
use Illuminate\Database\Seeder;

class PerkHasCategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Factory::create('fr_FR');
        $perks = Perk::all();
        $categoriesIds = Category::all()->pluck('id')->toArray();

        foreach($perks as $perk) {
            $categories = $faker->randomElements($categoriesIds, rand(1, count($categoriesIds)), false);

            foreach($categories as $category) {
                DB::table('perk_has_categories')->insert([
                    'perk_id' => $perk->id,
                    'category_id' => $category
                ]);
            }
        }
    }
}
