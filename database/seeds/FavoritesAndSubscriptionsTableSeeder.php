<?php

use App\Enums\Permission;
use App\Enums\UserRole;
use App\Models\{ Perk, User };
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class FavoritesAndSubscriptionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = \Faker\Factory::create('fr');
        $users = User::all();
        $tables = [
            [
                'name' => 'subscriptions',
                'permission' => Permission::SHOW_PERKS
            ],
            [
                'name' => 'favorites',
                'permission' => Permission::SHOW_PERKS
            ]
        ];
        $perks = Perk::all()->pluck('id')->all();

        foreach ($tables as $table) {
            foreach($users as $user) {
                if($user->hasPermissionTo($table['permission'])) {
                    $perksIds = $faker->randomElements(
                        $perks, rand(1, 5), false
                    );
                    foreach($perksIds as $perkId) {
                        DB::table($table['name'])->insert([
                            'user_id' => $user->id,
                            'perk_id' => $perkId,
                            'created_at' => $faker->dateTimeThisYear,
                            'updated_at' => $faker->dateTimeThisYear
                        ]);
                    }
                }
            }
        }
    }
}
