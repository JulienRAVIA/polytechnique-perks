<?php

use App\Enums\UserStatus;
use App\Models\User;
use Illuminate\Database\Seeder;

class MainUsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $roles = [
            'admin',
            'startup',
            'company'
        ];

        foreach ($roles as $role) {
            $user = User::create([
                'firstname' => 'Polytechnique',
                'lastname' =>  'PERKS',
                'company_name' => 'École polytechnique',
                'email' => "{$role}@{$role}.com",
                'password' => Hash::make($role), // password
                'remember_token' => Str::random(10),
                'status' => UserStatus::VALIDATED,
            ]);
            $user->assignRole($role);
        }
    }
}
