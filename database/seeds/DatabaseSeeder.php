<?php

use App\Models\{ Category, Sector };
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            CollaborationSeeder::class,
            CategoriesTableSeeder::class,
            RoleAndPermissionsTableSeeder::class,
            MainUsersTableSeeder::class,
            UsersAndPerksTableSeeder::class,
            RatingTableSeeder::class,
            FavoritesAndSubscriptionsTableSeeder::class,
            PerkHasCategoriesTableSeeder::class,
        ]);
    }
}
