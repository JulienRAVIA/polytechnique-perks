<?php

use App\Enums\Permission;
use App\Models\Perk;
use App\Models\User;
use Illuminate\Database\Seeder;

class RatingTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = \Faker\Factory::create('fr');
        $users = User::permission(Permission::SHOW_PERKS)->get();
        $perks = Perk::all()->pluck('id')->all();

        foreach($users as $user) {
            $perksIds = $faker->randomElements(
                $perks, rand(1, 10), false
            );
            foreach($perksIds as $perkId) {
                DB::table('ratings')->insert([
                    'user_id' => $user->id,
                    'perk_id' => $perkId,
                    'note' => $faker->numberBetween(1, 5),
                    'title' => $faker->realText(35),
                    'comment' => $faker->paragraph,
                    'created_at' => $faker->dateTimeThisYear,
                    'updated_at' => $faker->dateTimeThisYear
                ]);
            }
        }
    }
}
