<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
use App\Enums\Permission as PermissionEnum;

class RoleAndPermissionsTableSeeder extends Seeder
{
    const ADMINISTRATOR     = 'admin';
    const STARTUP_MEMBER    = 'startup';
    const COMPANY           = 'company';

    public static $roles = [
        self::COMPANY,
        self::STARTUP_MEMBER,
        self::ADMINISTRATOR,
    ];

    protected static $permissions = [
        PermissionEnum::PROPOSE_PERK        => [self::COMPANY],
        PermissionEnum::SHOW_PERKS         => [self::STARTUP_MEMBER],
        PermissionEnum::MANAGE_PLATFORM    => [self::ADMINISTRATOR],
        PermissionEnum::BE_EXPORTED        => [self::STARTUP_MEMBER, self::COMPANY]
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach(static::$roles as $name) {
            Role::create([
                'name' => $name,
            ]);
        }

        foreach(self::$permissions as $permissionName => $roles) {
            $permission = Permission::create([
                'name' => $permissionName
            ]);
            foreach ($roles as $role) {
                $permission->assignRole($role);
            }
        }
    }
}
