<?php

use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $categories = [
            'HR',
            'Sales',
            'Marketing/Communication',
            'Tech/IT',
            'Design',
            'Finance/Accounting',
            'Legal',
            'Office management',
        ];
        $date = new DateTime();

        foreach($categories as $category) {
            DB::table('categories')->insert([
                'label' => $category,
                'created_at' => $date,
                'updated_at' => $date,
            ]);
        }
    }
}
