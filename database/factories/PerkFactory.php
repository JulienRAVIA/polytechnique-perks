<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Enums\PerkStatus;
use App\Models\Category;
use App\Models\Perk;
use App\Models\Sector;
use App\Models\User;
use Faker\Generator as Faker;

$factory->define(Perk::class, function (Faker $faker) {
    return [
        'title' => $faker->text(50),
        'tagline' => $faker->text(150),
        'contact_email_address' => $faker->safeEmail,
        'website_url' => $faker->url,
        'description' => $faker->realText(200),
        'subscription_conditions' => $faker->realText(500),
        'offer' => $faker->text(35),
        'status' => PerkStatus::getRandomValue(),
        'clicks_number' => $faker->numberBetween(0, 500),
        'created_at' => $faker->dateTime(),
        'updated_at' => $faker->dateTime(),
        'expires_at' => $faker->dateTimeBetween('-5 months', '+2 years'),
    ];
});
