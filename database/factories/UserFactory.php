<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Enums\UserStatus;
use Illuminate\Support\Facades\Hash;
use App\Models\{ Perk, User };
use Illuminate\Support\Str;
use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(User::class, function (Faker $faker, $data) {
    static $i = 1;
    return [
        'firstname' => $faker->firstName,
        'lastname' => $faker->lastName,
        'company_name' => $faker->company,
        'email' => "user_".$i++."@tcm.com",
        'password' => Hash::make('password'), // password
        'remember_token' => Str::random(10),
        'status' => $data['status'] ?? UserStatus::getRandomValue(),
    ];
});
