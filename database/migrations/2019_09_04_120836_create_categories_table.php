<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('categories', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('label');
            $table->timestamps();
        });

        Schema::create('perk_has_categories', function (Blueprint $table) {
            $table->bigInteger('category_id')
                ->unsigned()
                ->index();
            $table->foreign('category_id')
                ->references('id')
                ->on('categories')
                ->onDelete('cascade');
            $table->bigInteger('perk_id')
                ->unsigned()
                ->index();
            $table->foreign('perk_id')
                ->references('id')
                ->on('perks')
                ->onDelete('cascade');
            $table->primary(['perk_id', 'category_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('perk_has_categories');
        Schema::dropIfExists('categories');
    }
}
