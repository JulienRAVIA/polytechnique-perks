<?php

use App\Enums\PerkStatus;
use App\Models\Perk;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePerksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('perks', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('title');
            $table->string('tagline')
                ->nullable();
            $table->string('contact_email_address');
            $table->string('website_url')
                ->nullable();
            $table->longText('description');
            $table->longText('subscription_conditions')
                ->nullable();
            $table->boolean('is_published')
                ->default(1);
            $table->string('status')
                ->default(Perk::getDefaultStatus());
            $table->integer('clicks_number')
                ->default(0);
            $table->string('offer', 50)->nullable();
            $table->timestamps();
            $table->timestamp('expires_at', 0)
                ->nullable();
            $table->softDeletes();
            $table->bigInteger('user_id')
                ->unsigned()
                ->index();
            $table->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('perks');
    }
}
