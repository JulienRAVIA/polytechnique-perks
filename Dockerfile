FROM thecodingmachine/php:7.3-v2-apache-node10

COPY --chown=docker:docker . .

ENV PHP_EXTENSIONS="gd zip"

RUN composer install
RUN yarn
RUN yarn production
RUN php artisan storage:link
RUN sudo chown -R docker:docker /var/www/html/storage/app
