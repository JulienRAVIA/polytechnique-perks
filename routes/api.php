<?php

use App\Enums\Permission;
use App\Http\Resources\UserResource;
use Illuminate\Http\Request;
use const App\Http\Controllers\PerkController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/**
 * Guest routes
 */
Route::group(['middleware' => ['api']], function () {
    /**
     * Login user with mail and password
     */
    Route::post('/login', 'Auth\LoginController@login');

    /**
     * Create an account for startup or company
     */
    Route::post('/register', 'Auth\RegisterController@register');

    /**
     * Forgotten password: ask for reset link
     */
    Route::post('/password/forgot', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.forgot');

    /**
     * Reset password
     */
    Route::post('/password/reset', 'Auth\ResetPasswordController@reset');

    /**
     * Get collaboration list
     */
    Route::get('/collaborations', 'CollaborationController@all');
});

/**
 * Private routes
 */
Route::group(['middleware' => ['auth', 'api']], function () {
    /**
     * Connected user related routes
     */
    Route::prefix('me')->group(function () {
        /**
         * Fetch connected user data
         */
        Route::get('', function (Request $request) {
            return new UserResource($request->user());
        });

        /**
         * Logout connected user
         */
        Route::post('/logout', 'Auth\LoginController@logout');

        /**
         * Get relation between a user and a perk
         */
        Route::get('/perks', 'UserController@listPerks');

        /**
         * Get relation between a user and a perk
         */
        Route::get('/perks/{perk}', 'UserController@perk');
    });


    /**
     * See published perks
     */
    Route::get('/perks/published', 'PerkController@getPublished');

    /**
     * Manage platform routes (admin related)
     */
    Route::group(['middleware' => ['permission:' . Permission::MANAGE_PLATFORM]], function () {
        Route::prefix('users')->group(function () {
            /**
             * Validate / Unvalidate user account
             */
            Route::post('/{user}/validation', 'UserController@changeStatus');

            /**
             * Get all companies
             */
            Route::get('/companies', 'UserController@listCompanies');

            /**
             * Restore user
             */
            // todo Deleted users are not restorable
            Route::patch('/{user}/restore', 'UserController@restore');

            /**
             * Export users to .csv
             */
            Route::get('/export', 'UserController@export');

            /**
             * Get one user
             */
            Route::get('/{user}', 'UserController@get');

            /**
             * Delete user
             */
            Route::delete('/{user}', 'UserController@delete');

            /**
             * Get all users
             */
            Route::get('/', 'UserController@list');

            // todo gestion des comptes startup et company

            // todo route pour créer des admin
        });

        Route::prefix('perks')->group(function () {
            /**
             * Get all perks
             */
            Route::get('/all', 'PerkController@list');

            /**
             * Delete perk
             */
            Route::delete('/{perk}/delete', 'PerkController@delete');

            /**
             * Restore perk
             */
            // todo Deleted perks are not restorable
            Route::patch('/{perks}/restore', 'PerksController@restore');

            /**
             * Validate / Unvalidate perk
             */
            Route::post('/{perk}/validation', 'PerkController@changeStatus');
        });
    });

    /**
     * Perk management for startup
     */
    Route::group(['middleware' => ['permission:' . Permission::SHOW_PERKS]], function () {
        Route::prefix('perks')->group(function () {
            /**
             * Subscribe to a perk
             */
            Route::post('/{perk}/subscribe', 'PerkController@subscribe');

            /**
             * Rate a perk
             */
            Route::post('/{perk}/rating', 'PerkController@addRating');

            /**
             * Set/unset perk as favorite
             */
            Route::put('/{perk}/favorite', 'PerkController@setFavorite');

            // todo gestion du compte startup
        });
    });

    /**
     * Perk management for companies
     */
    Route::group(['middleware' => ['permission:' . Permission::PROPOSE_PERK]], function () {
        Route::prefix('perks')->group(function () {
            // todo gestion du compte company
        });
    });

    /**
     * Common routes between admin and companies
     */
    Route::group([
        'middleware' => [
            'permission:' . Permission::PROPOSE_PERK . '|' . Permission::MANAGE_PLATFORM
        ]], function () {
            Route::prefix('perks')->group(function () {
                /**
                 * Get perk statistics
                 */
                Route::get('/{perk}/statistics', 'PerkController@getStatistics');

                /**
                 * Create a perk
                 */
                Route::post('/', 'PerkController@create');

                /**
                 * Delete one of my perks
                 */
                Route::delete('/{perk}', 'PerkController@delete');

                /**
                 * Renew a perk
                 */
                Route::post('/{perk}/renew', 'PerkController@renew');

                /**
                 * Modify a perk
                 */
                Route::post('/{perk}', 'PerkController@update');
            });
        });

    /**
     * Get perk
     */
    Route::get('/perks/{perk}', 'PerkController@get');

    /**
     * List all categories
     */
    Route::get('/categories', 'CategoryController@list');

    /**
     * Get perk ratings
     */
    Route::get('perks/{perk}/ratings', 'PerkController@getRating');
});
