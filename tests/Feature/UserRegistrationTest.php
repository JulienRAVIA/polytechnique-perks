<?php

namespace Tests\Feature;

use App\Enums\UserRole;
use App\Enums\UserStatus;
use App\Models\Collaboration;
use App\Models\User;
use Illuminate\Foundation\Testing\TestResponse;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Hash;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

/**
 * Class UserRegistrationTest
 * @package Tests\Feature
 */
class UserRegistrationTest extends TestCase
{
    const STARTUP_MEMBER = 'startup';
    const ADMINISTRATOR = 'admin';
    const COMPANY = 'company';

    const DEFAULT_PASSWORD = 'password';

    use WithFaker;
    use RefreshDatabase;

    /**
     * @var User
     */
    protected $user;

    /**
     * @var User
     */
    protected $admin;

    /**
     * @var User
     */
    protected $startupMember;


    public static function setUpBeforeClass(): void
    {
        parent::setUpBeforeClass();
    }

    public function setUp(): void
    {
        parent::setUp();

        Artisan::call('db:seed --class=RoleAndPermissionsTableSeeder');
        Artisan::call('db:seed --class=CollaborationSeeder');
        $this->user = factory(User::class, 1)
            ->create(['status' => UserStatus::VALIDATED])->first()->assignRole(self::COMPANY);
        $this->admin = factory(User::class, 1)
            ->create(['status' => UserStatus::VALIDATED])->first()->assignRole(self::ADMINISTRATOR);
        $this->startupMember = factory(User::class, 1)
            ->create(['status' => UserStatus::VALIDATED])->first()->assignRole(self::STARTUP_MEMBER);
    }

    /**
     * Make login request
     *
     * @param string $email
     * @param string $password
     * @return TestResponse
     */
    protected function login(string $email, string $password = self::DEFAULT_PASSWORD): TestResponse
    {
        return $this->json('POST', '/api/login', [
            'email' => $email,
            'password' => $password
        ]);
    }

    /**
     * Make create account request
     *
     * @param array $datas
     * @return TestResponse
     */
    protected function createUser(array $datas)
    {
        return $this->json('POST', '/api/register', array_merge([
            'email' => $this->faker->email,
            'password' => self::DEFAULT_PASSWORD,
            'company_name' => $this->faker->company,
            'firstname' => $this->faker->firstName,
            'lastname' => $this->faker->lastName,
            'conditions' => true,
        ], $datas));
    }

    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testLoginWithGoodCredentials(): void
    {
        $this->login($this->user->email)->assertStatus(200);
    }

    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testLoginWithBadCredentials(): void
    {
        $this->login($this->user->email, 'bad_password')->assertStatus(422);
    }

    /**
     * We test if the startup member created when we're guest is a startup member (and not admin
     *
     * @return void
     */
    public function testCreateStartupMemberAsGuest(): void
    {
        $this->createUser([
            'role' => self::STARTUP_MEMBER,
            'propose_perk' => 'no',
            'collaboration'     => Collaboration::first()->getKey()
        ])->assertStatus(201)->assertJsonFragment([
            'role' => self::STARTUP_MEMBER,
            'status' => UserStatus::PENDING
        ]);
    }

    /**
     * We test if the admin created when we're admin is an admin
     */
    // todo pour l'instant on a pas de route pour creer un admin
    /*public function testCreateAdminAsAdmin()
    {
        $this->login($this->admin->email)->assertStatus(200);
        $this->get('/api/me');
        $this->createUser([
            'role' => self::ADMINISTRATOR
        ])->assertStatus(201)->assertJsonFragment([
            'role' => self::ADMINISTRATOR,
            'status' => UserStatus::VALIDATED
        ]);
    }*/

    /**
     * We test if the admin created when we're not admin is a startup member (and not admin)
     */
    public function testCreateCompanyAsGuest()
    {
        $this->createUser([
            'role' => self::COMPANY,
            'propose_perk' => 'yes',
            'logo' => UploadedFile::fake()->image('logo.jpg'),
        ])
        ->assertStatus(201)->assertJsonFragment([
            'role' => self::COMPANY,
            'status' => UserStatus::PENDING
        ]);
    }
}
