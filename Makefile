TARGETS:=$(MAKEFILE_LIST)

DC=docker-compose
CONTAINER=app

RUNNING:=$(shell docker ps -q --filter status=running --filter name=^/$(COMPOSE_PROJECT_NAME) | xargs)
MYSQL_VOLUME := $(shell docker volume ls --quiet --filter name=$(COMPOSE_PROJECT_NAME)_mysql)
.DEFAULT_GOAL := general

.PHONY: help
help: ## Display commands informations
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(TARGETS) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'

.PHONY: install
install: ## Install containers and copy env files
	cp .env.example .env
	$(DC) up

.PHONY: start
start: ## Start the containers application
ifeq ($(RUNNING),)
	$(DC) up -d
else
	$(DC) ps
endif

.PHONY: stop
stop:  ## Stop the containers application
ifeq ($(RUNNING),)
	$(DC) stop
else
	$(DC) ps
endif

.PHONY: restart
restart: ## Relaunch the containers application
ifeq ($(RUNNING),)
	$(MAKE) stop --no-print-directory
endif
	$(MAKE) start --no-print-directory

.PHONY: artisan
artisan: ## Launch artisan command (make artisan CMD=view:clear)
	$(DC) exec app php artisan $(CMD)

.PHONY: bash
bash: ## Gives access to container bash (make bash CONTAINER=mysql or make bash (for app container by default))
	$(DC) exec $(CONTAINER) bash

.PHONY: yarn
yarn: ## Gives access to container bash (make bash CONTAINER=mysql or make bash (for app container by default))
	$(DC) exec $(CONTAINER) yarn watch-poll

.PHONY: sh
sh: ## Gives access to container bash (make sh CONTAINER=mysql or make sh (for app container by default))
	$(DC) exec $(CONTAINER) sh

.PHONY: check
check: ## Gives access to container bash (make sh CONTAINER=mysql or make sh (for app container by default))
	$(MAKE) check-php
	$(MAKE) check-js

.PHONY: check-php
check-php: ## Gives access to container bash (make sh CONTAINER=mysql or make sh (for app container by default))
	$(DC) exec $(CONTAINER) composer cs-check
	$(DC) exec $(CONTAINER) composer phpstan
	$(DC) exec $(CONTAINER) composer nc-tests

.PHONY: check-js
check-js: ## Gives access to container bash (make sh CONTAINER=mysql or make sh (for app container by default))
	$(DC) exec $(CONTAINER) yarn lint

.PHONY: fix-js
fix-js: ## Gives access to container bash (make sh CONTAINER=mysql or make sh (for app container by default))
	$(DC) exec $(CONTAINER) yarn lint --fix

# .PHONY: migrate
# migrate: ## Make migrate action (make migrate ACTION=refresh)
# ifeq ($(ACTION),)
# 	$(MAKE) artisan CMD=migrate --no-print-directory
# else
# 	$(MAKE) artisan CMD=migrate:$(ACTION) --no-print-directory
# endif

# .PHONY: general
# general: ## Make global action (make NS=migrate ACTION=refresh or make NS=migrate will display actions list for migrate)
# ifneq ($(NS),)
# ifneq ($(ACTION),)
# 	$(MAKE) artisan CMD=$(NS):$(ACTION) --no-print-directory
# else
# 	@echo "You must provide action, actions availables will be displayed"
# 	$(DC) exec app php artisan list $(NS)
# endif
# else
# 	$(MAKE) help --no-print-directory
# endif

# .PHONY: class
# class: ## Generates class (make class TYPE=model NAME=Post)
# ifeq ($(TYPE),)
# 	$(DC) exec app php artisan list make
# else
# ifeq ($(NAME),)
# 	@echo 'You must provide filename'
# else
# 	$(DC) exec app php artisan make:$(TYPE) $(NAME)
# endif
# endif
