stages:
  - lint
  - build
  - deploy
  - cleanup

lint_app:
  stage: lint
  image: thecodingmachine/php:7.3-v2-apache-node10
  cache:
    paths:
      - node_modules
      - api/vendor
      - $HOME/.composer
  before_script:
    - yarn
    - sh ci/install.sh
  script:
    - yarn lint
    - composer cs-check
    - composer phpstan -- --memory-limit=1024M
  variables:
    PHP_INI_MEMORY_LIMIT: 1g
    PHP_EXTENSION_GD: 1

test_app:
  stage: lint
  image: thecodingmachine/php:7.3-v2-cli
  cache:
    paths:
    - api/vendor
    - $HOME/.composer
  services:
  - mysql:5.7
  variables:
    PHP_EXTENSION_XDEBUG: 1
    PHP_EXTENSION_GD: 1
    MYSQL_DATABASE: api
    MYSQL_ROOT_PASSWORD: secret
    MYSQL_USER: api
    MYSQL_PASSWORD: secret
    DB_HOST: mysql
    DB_DATABASE: api
    DB_USERNAME: api
    DB_PASSWORD: secret
    APP_ENV: testing
  before_script:
  - sh ci/install-test.sh
  script:
  - composer tests
  after_script:
  - /home/docker/washingmachine/washingmachine run -v --clover=api/clover.xml
  artifacts:
    when: always
    expire_in: 1 month
    paths:
    - api/coverage
    - api/clover.xml

.build-image:
  stage: build
  image: docker:git
  services:
  - docker:dind
  variables:
    DOCKER_DRIVER: overlay2
  script:
  - docker login -u gitlab-ci-token -p $CI_BUILD_TOKEN git.thecodingmachine.com:444
  - docker build -t git.thecodingmachine.com:444/tcm-projects/polytechnique-perk:${CI_COMMIT_REF_SLUG} -f Dockerfile .
  - docker push git.thecodingmachine.com:444/tcm-projects/polytechnique-perk:${CI_COMMIT_REF_SLUG}

build_app:
  extends: .build-image
  only:
  - branches
  except:
  - master
  - develop

deploy_branches:
  image: lwolf/kubectl_deployer:latest
  stage: deploy
  variables:
    KUBECONFIG: /root/.kube/config
  before_script:
  - apk update && apk add curl
  - mkdir ~/.kube
  - echo "$KUBE_CONFIG" > ~/.kube/config
  - kubectl create namespace ${CI_PROJECT_PATH_SLUG}-${CI_COMMIT_REF_SLUG} || true
  - kubectl -n ${CI_PROJECT_PATH_SLUG}-${CI_COMMIT_REF_SLUG} delete all --all
  - kubectl -n ${CI_PROJECT_PATH_SLUG}-${CI_COMMIT_REF_SLUG} delete pvc mysql-data || true
  - kubectl -n ${CI_PROJECT_PATH_SLUG}-${CI_COMMIT_REF_SLUG} delete pvc api-data || true
  - kubectl -n ${CI_PROJECT_PATH_SLUG}-${CI_COMMIT_REF_SLUG} delete secret tcmregistry || true
  - cd kubernetes
  script:
  - kubectl -n ${CI_PROJECT_PATH_SLUG}-${CI_COMMIT_REF_SLUG} create secret docker-registry tcmregistry --docker-username=ci.gitlab@thecodingmachine.com --docker-password=$CI_PASSWD --docker-server=git.thecodingmachine.com:444 --docker-email=ci.gitlab@thecodingmachine.com
  - sed -i "s/#ENVIRONMENT#/${CI_COMMIT_REF_SLUG}/g" config.yaml
  - kubectl -n ${CI_PROJECT_PATH_SLUG}-${CI_COMMIT_REF_SLUG} apply -f .
  - curl "https://bigbro.thecodingmachine.com/gitlab/call/start-environment?projectId=${CI_PROJECT_ID}&commitSha=${CI_COMMIT_SHA}&ref=${CI_COMMIT_REF_NAME}&name=${CI_PROJECT_PATH_SLUG}-${CI_COMMIT_REF_SLUG}"
  environment:
    name: review/$CI_COMMIT_REF_NAME
    url: https://bigbro.thecodingmachine.com/environment/${CI_PROJECT_PATH_SLUG}-${CI_COMMIT_REF_SLUG}
    on_stop: cleanup_branches
  when: manual
  only:
  - branches
  except:
  - master
  - staging
  - develop

deploy_staging:
  image: lwolf/kubectl_deployer:latest
  stage: deploy
  variables:
    KUBECONFIG: /root/.kube/config
  before_script:
    - apk update && apk add curl
    - mkdir ~/.kube
    - echo "$KUBE_CONFIG" > ~/.kube/config
    - kubectl create namespace ${CI_PROJECT_PATH_SLUG}-${CI_COMMIT_REF_SLUG} || true
    - kubectl -n ${CI_PROJECT_PATH_SLUG}-${CI_COMMIT_REF_SLUG} delete all --all
    - kubectl -n ${CI_PROJECT_PATH_SLUG}-${CI_COMMIT_REF_SLUG} delete secret tcmregistry || true
    - cd kubernetes
  script:
    - kubectl -n ${CI_PROJECT_PATH_SLUG}-${CI_COMMIT_REF_SLUG} create secret docker-registry tcmregistry --docker-username=ci.gitlab@thecodingmachine.com --docker-password=$CI_PASSWD --docker-server=git.thecodingmachine.com:444 --docker-email=ci.gitlab@thecodingmachine.com
    - sed -i "s/#ENVIRONMENT#/${CI_COMMIT_REF_SLUG}/g" config.yaml
    - kubectl -n ${CI_PROJECT_PATH_SLUG}-${CI_COMMIT_REF_SLUG} apply -f .
    - curl "https://bigbro.thecodingmachine.com/gitlab/call/start-environment?projectId=${CI_PROJECT_ID}&commitSha=${CI_COMMIT_SHA}&ref=${CI_COMMIT_REF_NAME}&name=${CI_PROJECT_PATH_SLUG}-${CI_COMMIT_REF_SLUG}"
  environment:
    name: review/$CI_COMMIT_REF_NAME
    url: https://bigbro.thecodingmachine.com/environment/${CI_PROJECT_PATH_SLUG}-${CI_COMMIT_REF_SLUG}
    on_stop: cleanup_staging
  when: manual
  only:
    - staging

cleanup_branches:
  stage: cleanup
  image: thecodingmachine/gitlab-registry-cleaner:latest
  variables:
    KUBECONFIG: /root/.kube/config
    GIT_STRATEGY: none
  before_script:
  - apk update && apk add curl
  - mkdir ~/.kube
  - echo "$KUBE_CONFIG" > ~/.kube/config
  script:
  - /delete_image.sh git.thecodingmachine.com:444/tcm-projects/polytechnique-perk:${CI_COMMIT_REF_SLUG}
  - kubectl -n ${CI_PROJECT_PATH_SLUG}-${CI_COMMIT_REF_SLUG} delete all --all
  - kubectl -n ${CI_PROJECT_PATH_SLUG}-${CI_COMMIT_REF_SLUG} delete pvc mysql-data || true
  - kubectl -n ${CI_PROJECT_PATH_SLUG}-${CI_COMMIT_REF_SLUG} delete secret tcmregistry || true
  - kubectl delete namespace ${CI_PROJECT_PATH_SLUG}-${CI_COMMIT_REF_SLUG}
  - curl "https://bigbro.thecodingmachine.com/gitlab/call/stop-environment?projectId=${CI_PROJECT_ID}&commitSha=${CI_COMMIT_SHA}&name=${CI_PROJECT_PATH_SLUG}-${CI_COMMIT_REF_SLUG}"
  when: manual
  environment:
    name: review/$CI_COMMIT_REF_NAME
    action: stop
  only:
  - branches
  except:
  - master
  - staging
  - develop

cleanup_staging:
  stage: cleanup
  image: thecodingmachine/gitlab-registry-cleaner:latest
  variables:
    KUBECONFIG: /root/.kube/config
    GIT_STRATEGY: none
  before_script:
  - apk update && apk add curl
  - mkdir ~/.kube
  - echo "$KUBE_CONFIG" > ~/.kube/config
  script:
  - /delete_image.sh git.thecodingmachine.com:444/tcm-projects/polytechnique-perk:${CI_COMMIT_REF_SLUG}
  - kubectl -n ${CI_PROJECT_PATH_SLUG}-${CI_COMMIT_REF_SLUG} delete all --all
  - kubectl -n ${CI_PROJECT_PATH_SLUG}-${CI_COMMIT_REF_SLUG} delete pvc mysql-data || true
  - kubectl -n ${CI_PROJECT_PATH_SLUG}-${CI_COMMIT_REF_SLUG} delete pvc api-data || true
  - kubectl -n ${CI_PROJECT_PATH_SLUG}-${CI_COMMIT_REF_SLUG} delete secret tcmregistry || true
  - kubectl delete namespace ${CI_PROJECT_PATH_SLUG}-${CI_COMMIT_REF_SLUG}
  - curl "https://bigbro.thecodingmachine.com/gitlab/call/stop-environment?projectId=${CI_PROJECT_ID}&commitSha=${CI_COMMIT_SHA}&name=${CI_PROJECT_PATH_SLUG}-${CI_COMMIT_REF_SLUG}"
  when: manual
  environment:
    name: review/$CI_COMMIT_REF_NAME
    action: stop
  only:
  - staging


